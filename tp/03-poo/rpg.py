from arme import Arme
from personnage import Personnage
from arbitre import Arbitre

épée = Arme("son épée Narsil", 6, 18)
hache = Arme("sa hache de guerre", 1, 21)
aragorn = Personnage("Aragorn", 120, épée)
orc = Personnage("Ograukh", 140, hache)

#print(aragorn)
#print(orc)

mdj = Arbitre(aragorn, orc)
mdj.combat()