class Arbitre:
    """
    Je suis une classe permettant de simuler un combat 
    entre deux personnages armés.
    
    Le combat se déroule au tour par tour alternatif, 
    et s'arrête à la mort d'un des personnages (lorsque 
    ses pv sont égaux à 0).
    """
    
    def __init__(self, p1, p2):
        self._p1 = p1
        self._p2 = p2
        
    def personnage1(self):
        return self._p1
        
    def personnage2(self):
        return self._p2
        
    def un_round(self):
        """
        Simule *un* round du combat.
        
        Le premier personnage frappe le second à l'aide 
        de son arme.
        
        Puis, si le second est encore vivant, il frappe 
        à son tour le premier personnage.
        
        Affiche des messages d'informations dans la console.
        """
    
        a1 = self._p1.arme()
        d1 = a1.dégât_aléatoire()
        self._p2.blesse(d1)
        print("{} inflige {} points de dégâts à {} avec {}".format(self._p1.nom(),
                                                                   d1,
                                                                   self._p2.nom(),
                                                                   a1.nom()))

        if self._p2.est_vivant():
            a2 = self._p2.arme()
            d2 = a2.dégât_aléatoire()
            self._p1.blesse(d2)
            print("{} inflige {} points de dégâts à {} avec {}".format(self._p2.nom(),
                                                                    d2,
                                                                    self._p1.nom(),
                                                                    a2.nom()))
                                                                    
    def combat(self):
        """
        Simule un combat complet: les rounds se répètent 
        jusqu'à ce que l'un des personnages soit mort.

        
        Affiche des messages d'informations dans la console.
        """
        
        print("Un combat s'engage entre {} et {}.".format(self._p1.nom(),
                                                         self._p2.nom()))
        print("{} prend l'initiative et frappe en premier.".format(self._p1.nom()))
        
        r = 1
        while self._p1.est_vivant() and self._p2.est_vivant():
            print("\n--------------")
            print("Round n°{}\n".format(r))
            r += 1
            
            self.un_round()
            
            print("\nFin du round: {} ({} pv) contre {} ({} pv)".format(self._p1.nom(),
                                                                    self._p1.points_de_vie(),
                                                                    self._p2.nom(),
                                                                    self._p2.points_de_vie()))
            