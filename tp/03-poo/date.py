def bissextile(année):
  return année % 4 == 0 and année % 100 != 0 or année % 400 == 0

class Date:
  
  def __init__(self, jour, mois, année):
    self._nbr_jours = [31, 28, 31, 30, 31, 30,
                      31, 31, 30, 31, 30, 31]
                      
    if mois not in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]:
      raise ValueError("Le mois doit être un entier entre 1 et 12")
    
    if mois == 2 and bissextile(année):
      dernier_jour = 29
    else:
      # Attention: tableaux indexés à partir de 0 en python
      dernier_jour = self._nbr_jours[mois-1]
             
    if jour < 1 or jour > dernier_jour:
      raise ValueError("Un jour dans le mois {} doit être un entier entre 1 et {}".format(mois, dernier_jour))
    
    self.jour = jour
    self.mois = mois
    self.année = année
    
  def __str__(self):
    noms_mois = ["janvier", "février", "mars", 
                 "avril", "mai", "juin",
                 "juillet", "août", "septembre",
                 "octobre", "novembre", "décembre"
                ]
                
    return "{}{} {} {}".format(self.jour, 
                               "er" if self.jour == 1 else "",
                               noms_mois[self.mois - 1], 
                               self.année)
                             
  def __repr__(self):
    return "Date({}, {}, {})".format(self.jour, 
                                     self.mois,
                                     self.année)
    
  def _jours_depuis_1er_janvier(self):
    """
    Calcule le nombre de jours entre la date self (exclu)
    et le 1er janvier de la même année (inclus)
    """
  
    # On additionne le nbr de jours de tous le mois qui précèdent
    # self.mois
    
    somme = 0
    for i in range(1, self.mois):
      somme += self._nbr_jours[i - 1]
    if bissextile(self.année) and self.mois > 2:
      somme += 1
      
    # On rajoute le nbr de jours du mois (sauf le jour self)
    somme += self.jour - 1
    
    return somme
    
  def _nombre_jours_entre_deux_1er_janvier(self, année1, année2):
    début = min(année1, année2)
    fin = max(année1, année2)
    
    somme = 0
    for a in range(début, fin):
      somme += 365
      if bissextile(a):
        somme += 1

    if année1 <= année2:
      return somme
    else:
      return -somme
      
  def __sub__(self, autre_date):
    n1 = self._jours_depuis_1er_janvier()
    n2 = autre_date._jours_depuis_1er_janvier()
    
    n = self._nombre_jours_entre_deux_1er_janvier(self.année, autre_date.année)
    
    return -n + n1 - n2