from random import randint

class Arme:
    """
    Je suis un objet permettant d'infliger des dégâts. J'ai un 
    nom, ainsi qu'un intervalle de dégâts possibles.
    """
  
    def __init__(self, nom, dégâts_min, dégâts_max):
        self._nom = nom
        self._dégâts_min = dégâts_min
        self._dégâts_max = dégâts_max
    
    def nom(self):
        return self._nom
    
    def dégât_aléatoire(self):
        """
        Renvoie un entier aléatiore de dégâts infligés par 
        cette arme, compris dans l'intervalle de dégâts 
        possibles.
        """
        
        return randint(self._dégâts_min, self._dégâts_max)
        
    def __str__(self):
        return "{} ({} -> {} dégâts)".format(self._nom,
                                             self._dégâts_min,
                                             self._dégâts_max)
