class Personnage:
    """
    Je suis un personnage de RPG. J'ai un nom, un nombre 
    de points de vie (entier >= 0): à 0 je suis mort. 
    
    J'ai aussi une arme qui me permet d'attaquer des 
    adversaires.
    """
  
    def __init__(self, nom, pv, arme):
        self._nom = nom
        self._pv = pv
        self._arme = arme
        
    def nom(self):
        return self._nom
    
    def points_de_vie(self):
        return self._pv
    
    def pv(self):
        return self.points_de_vie()
        
    def arme(self):
        return self._arme
        
    def est_vivant(self):
        """
        Renvoie True ssi le personnage est vivant, 
        False sinon.
        
        Un personnage est vivant ssi pv > 0.
        """
        
        return self._pv >= 0
    
    def blesse(self, dégâts):
        """
        Blesse le personnage en lui retirant 'dégâts' 
        points de vie.
        """
        
        self._pv -= dégâts
        
        return self._pv
        
    def __str__(self):
        return "{} ({} pts de vie) arme:{}".format(self._nom,
                                                   self._pv,
                                                   str(self._arme))