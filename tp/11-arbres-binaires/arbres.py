class Noeud:
   def __init__(self, valeur="", gauche=None, droite=None):
      assert gauche is None or isinstance(gauche, Noeud)
      assert droite is None or isinstance(droite, Noeud)
      
      self.gauche = gauche
      self.valeur = valeur
      self.droite = droite

   def __str__(self):
      if self.gauche is None:
         chaîne_gauche = ""
      else:
         chaîne_gauche = str(self.gauche)
      if self.droite is None:
         chaîne_droite = ""
      else:
         chaîne_droite = str(self.droite)

      return "({}{}{})".format(chaîne_gauche,
                               self.valeur,
                               chaîne_droite)

   def __repr__(self):
      if self.gauche is None and self.droite is None:
         # aucun sous-arbre
         return "Noeud({})".format(repr(self.valeur))
      elif self.gauche is None:
         # seulement un sous-arbre à droite
         return "Noeud({}, droite={})".format(repr(self.valeur),
                                              repr(self.droite))
      elif self.droite is None:
         # seulement un sous-arbre à droite
         return "Noeud({}, gauche={})".format(repr(self.valeur),
                                              repr(self.gauche))
      else:
         return "Noeud({}, {}, {})".format(repr(self.valeur),
                                           repr(self.gauche),
                                           repr(self.droite))
      
   def __eq__(self, autre):
      """
      Renvoie True ssi self est structurellement identique à l'arbre 'autre'.
      """

      if autre is None:
         return False # self ne peut jamais être None, puisque c'est un Noeud
      else:
         return (self.valeur == autre.valeur and
                 self.gauche == autre.gauche and
                 self.droite == autre.droite)

def arbre_graphviz(noeud, crée_noeud, crée_arête, noeud_central=None, largeur=None, hauteur=None, **kwdargs):
   def générateur_identifiant():
      i = 1
      while True:
         yield "id{}".format(i)
         i += 1

   identifiant = générateur_identifiant()

   def parcours(graphe, noeud, root=True, subgraph=None):

      id_noeud = next(identifiant)

      if noeud is None:
         if subgraph:
            crée_noeud(subgraph, id_noeud, noeud, vide=True)
         else:
            crée_noeud(graphe, id_noeud, noeud, vide=True)
         leaf = True
      else:
         if subgraph:
            crée_noeud(subgraph, id_noeud, noeud, vide=False, root=root)
         else:
            crée_noeud(graphe, id_noeud, noeud, vide=False, root=root)

         with graphe.subgraph() as sub:
            sub.attr(rank='same')
            
            id1, leaf1 = parcours(graphe, noeud.gauche, root=False, subgraph=sub)
            if noeud_central is not None:
               id_centre = next(identifiant)
               noeud_central(sub, id_centre)
            id2, leaf2 = parcours(graphe, noeud.droite, root=False, subgraph=sub)
            
            crée_arête(graphe, id_noeud, id1, gauche=True, feuille=leaf1)
            if noeud_central is not None:
               graphe.edge(id_noeud, id_centre, style="invis")
               graphe.edge(id1, id_centre, style="invis")
               graphe.edge(id_centre, id2, style="invis")
            crée_arête(graphe, id_noeud, id2, gauche=False, feuille=leaf2)

         leaf = False

      return id_noeud, leaf

   from graphviz import Graph

   g = Graph()
   
   if largeur is not None:
      largeur = float(largeur)*0.393701 # cm -> inches

      if hauteur is not None:
         hauteur = float(hauteur)*0.393701 # cm -> inches
         size = "{},{}".format(largeur, hauteur)
      else:
         size = "{}".format(largeur)
      g.graph_attr["size"] = size
   
   for key, value in kwdargs.items():
      g.graph_attr[key] = value

   parcours(g, noeud)
   
   return g
   

def graphe_arbre(noeud, 
                 bgcolor="lightgray", rootcolor="darkgrey", 
                 détaillé=False,
                 largeur=None, hauteur=None):

   from graphviz import nohtml
   
   def crée_noeud(graphe, id_noeud, noeud, vide=False, root=False):
      if vide:
         if not détaillé:
            graphe.node(id_noeud, label="", 
                        shape="rectangle", 
                        height="0.1",
                        style="invis")
      else:
         if root:
            largeur=",bold"
            color = rootcolor
         else:
            largeur=""
            color = bgcolor

         if not détaillé:            
            graphe.node(id_noeud, label=str(noeud.valeur), 
                        shape="rectangle", 
                        style="filled" + largeur,
                        height="0.3",
                        fillcolor=color)
         else:
            graphe.node(id_noeud, nohtml('<f0>g | <f1>{} | <f2>d'.format(noeud.valeur)),
                        shape="record",
                        style="filled" + largeur,
                        height="0.1",
                        fillcolor=color)
            
   def crée_arête(graphe, id1, id2, gauche=False, feuille=False):
      if détaillé:
         if gauche:
            id1 = id1 + ":f0:s"
         else:
            id1 = id1 + ":f2:s"
         id2 = id2 + ":f1:n"
         
      if feuille:
         if not détaillé:
            graphe.edge(id1, id2, style="dashed", color="slategrey")
      else:
         graphe.edge(id1, id2)

   def noeud_central(subgraph, id_centre):
      subgraph.node(id_centre, label="",
                    shape="square",
                    width="0.01",
                    style="invis")
      
   g = arbre_graphviz(noeud, crée_noeud, crée_arête,
                      noeud_central=noeud_central,
                      largeur=largeur, hauteur=hauteur)

   return g

def structure(noeud, bgcolor="lightgray", leafcolor="darkgray", largeur=None, hauteur=None):
   def crée_noeud(graphe, id_noeud, noeud, vide=False, root=False):
      if vide:
         graphe.node(id_noeud, label="", 
                     shape="square", 
                     width="0.1",
                     style="invis")
      else:
         graphe.node(id_noeud, label="", 
                     shape="square", 
                     style="filled",
                     width="0.1",
                     fillcolor=bgcolor)

   def noeud_central(subgraph, id_centre):
      subgraph.node(id_centre, label="",
                    shape="square",
                    width="0.01",
                    style="invis")

   def crée_arête(graphe, id1, id2, gauche=True, feuille=False):
      if gauche:
         id1 += ":sw"
         id2 += ":ne"
      else:
         id1 += ":se"
         id2 += ":nw"
         
      if feuille:
         graphe.edge(id1, id2, color=bgcolor)
      else:
         graphe.edge(id1, id2)
      
   g = arbre_graphviz(noeud, crée_noeud, crée_arête, 
                      noeud_central=noeud_central,
                      largeur=largeur,
                      hauteur=hauteur,
                      #splines="line",
                      ranksep="0.2",
                      nodesep="0.2",
                      )
   
   return g
