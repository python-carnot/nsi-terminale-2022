from listes import cellule, tête, queue, liste_vide, est_vide

class Pile:
   """
   Je représente une pile, implémentée grâce aux listes python.
   """
   
   def __init__(self):
      """
      Crée une pile vide
      """
      self._tableau = []

   def est_vide(self):
      """
      Renvoie True ssi je suis une pile vide.
      """
      return len(self._tableau) == 0

   def taille(self):
      """
      Renvoie ma taille (en nombre d'éléments empilés)
      """
      return len(self._tableau)

   def empiler(self, valeur):
      """
      Empile un nouvel élément en mon sommet.
      """
      self._tableau.append(valeur)

   def dépiler(self):
      """
      Dépile et renvoie la valeur de l'élément placé en mon
      sommet.

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Dépiler sur une pile vide")
      else:
         return self._tableau.pop()

   def sommet(self):
      """
      Renvoie la valeur de l'élément placé en mon
      sommet.

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Dépiler sur une pile vide")
      else:
         return self._tableau[self.taille() - 1]

   def vider(self):
      """
      Enlève tous les éléments empiles.
      """
      self._tableau = []

   def __str__(self):
      N = self.taille()
      
      chaîne = "("
      
      i = 0
      while i < N:
         chaîne += str(self._tableau[i])
         if i == N - 2:
            chaîne += " | "
         elif i < N - 2:
            chaîne += " "
         i += 1

      chaîne += ")"

      return chaîne

   def __repr__(self):
      return str(self)

class Pile2:
   """
   Je représente une pile, implémentée grâce à une
   liste chaînée immuable.
   """
   
   def __init__(self):
      """
      Crée une pile vide
      """
      self._liste = liste_vide()
      self._taille = 0

   def est_vide(self):
      """
      Renvoie True ssi je suis une pile vide.
      """
      return est_vide(self._liste)

   def taille(self):
      """
      Renvoie ma taille (en nombre d'éléments empilés)
      """
      return self._taille

   def empiler(self, valeur):
      """
      Empile un nouvel élément en mon sommet.
      """
      self._liste = cellule(valeur, self._liste)
      self._taille += 1

   def dépiler(self):
      """
      Dépile et renvoie la valeur de l'élément placé en mon
      sommet.

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Dépiler sur une pile vide")
      else:
         valeur = tête(self._liste)
         self._liste = queue(self._liste)
         self._taille -= 1
         return valeur

   def sommet(self):
      """
      Renvoie la valeur de l'élément placé en mon
      sommet.

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Dépiler sur une pile vide")
      else:
         return tête(self._liste)

   def vider(self):
      """
      Enlève tous les éléments empiles.
      """
      self._liste = liste_vide()
      self._taille = 0

   def __str__(self):
      chaîne = "("
      
      position = self._liste
      premier = True
      while not est_vide(position):
         chaîne += str(tête(position))
         if premier:
            chaîne += " | "
            premier = False
         elif not est_vide(queue(position)):
            chaîne += " "

         position = queue(position)

      chaîne += ")"

      return chaîne

   def __repr__(self):
      return str(self)
